<?php

/**
 * Class my_schemes
 *  Create the 'my schemes' admin template
 */
class my_schemes extends admin_template {

    /**
     * variables & constants
     */
    public static $_SLUG = 'acs-my_schemes';
    public static $_CLASS = 'my_schemes';


    /**
     * html
     *  Generate the html content of the template
     */
    public function html() {

        // Check if current page is valid
        if( !my_schemes::validate_page() )
            return;

        ?>

        <!-- Right column -->
        <script type="text/html" id="tpl-acs-col-right">
            <div id="acs-col-right">
                <div class="wp-box">
                    <div class="inner">
                        <h2><?php _e( "Advanced Color Schemes", acs_language::$_LANG ); ?></h2>

                        <ul>
                            <li><a href="http://www.advancedcustomfields.com/resources/#getting-started" target="_blank"><?php _e("Getting Started",'acf'); ?></a></li>
                        </ul>


                    </div>
                    <div class="footer footer-blue">
                        <ul class="hl">
                            <li><?php _e( "Created by", acs_language::$_LANG ); ?> JoCorpNet</li>
                        </ul>
                    </div>
                </div>
            </div>
        </script>

        <!-- jQuery action -->
        <script type="text/javascript">
            (function($){

                // wrap
                $('#wpbody .wrap').attr('id', 'acs-color_groups');
                $('#acs-color_groups').wrapInner('<div id="acs-col-left" />');
                $('#acs-color_groups').wrapInner('<div id="acs-cols" />');


                // add sidebar
                $('#acs-cols').prepend( $('#tpl-acs-col-right').html() );


                // take out h2 + icon
                $('#acs-col-left > .icon32').insertBefore('#acs-cols');
                $('#acs-col-left > h2').insertBefore('#acs-cols');

            })(jQuery);
        </script>

        <?php

    }


    /**
     * help
     *  Generate a help tap on the top right corner
     */
    public function help() {
        $screen = get_current_screen();

        // Help tab: Add new color schemes
        $screen->add_help_tab( array(
            'id'	=> self::$_SLUG . '-add_new_schemes',
            'title'	=> __( 'Add new schemes', acs_language::$_LANG ),
            'content'	=> '<p>' . __( 'Lorem ipsum.' ) . '</p>',
        ) );

        // Help tab: Remove color schemes
        $screen->add_help_tab( array(
            'id'	=> self::$_SLUG . '-remove_schemes',
            'title'	=> __( 'Remove schemes', acs_language::$_LANG ),
            'content'	=> '<p>' . __( 'Lorem ipsum.' ) . '</p>',
        ) );

        // Help tab: Share color schemes to network
        if( is_multisite() ) {
            $screen->add_help_tab(array(
                'id' => self::$_SLUG . '-share_network',
                'title' => __('Share to network', acs_language::$_LANG),
                'content' => '<p>' . __('Lorem ipsum.') . '</p>',
            ));
        }

    }


    /**
     * options
     *  Generate a options tab on the top right corner
     */
    public function options() {



    }


    /**
     * validate_page
     *  Check if current page is part of this plugin
     */
    public static function validate_page() {
        global $pagenow;

        if( in_array( $pagenow, array('edit.php') ) )
            if( isset($_GET['post_type']) && $_GET['post_type'] == acs::$_SLUG )
                return true;

        return false;
    }

}
<?php

/*
Plugin Name: Advanced Color Schemes
Plugin URI: http://bitbucket.jocorp.net/advanced-color-schemes
Description:
Version: 0.1
Author: JoCorpNet - Fabian Jocks
Author URI: http://bitbucket.jocorp.net/
Network: true
License: GPL2
*/

/**
 * IT'S ONLY FOR THE SECURITY
 */
defined( 'ABSPATH' ) or die( 'Access denied!' );


/**
 * Class acs
 *  The advanced color schemes plugin loader
 */
class acs {

    /**
     * Variables & constants
     */
    public static $_SLUG = 'acs';
    public static $_PATH = __FILE__;
    public static $_DEBUG = true;
    public $controller = array();


    /**
     * Constructor
     */
    public function __construct() {

        // Load all dependencies
        include_once( plugin_dir_path( acs::$_PATH ) . 'core/admin.php' );
        include_once( plugin_dir_path( acs::$_PATH ) . 'core/css.php' );
        include_once( plugin_dir_path( acs::$_PATH ) . 'core/language.php' );
        include_once( plugin_dir_path( acs::$_PATH ) . 'core/template.php' );
        include_once( plugin_dir_path( acs::$_PATH ) . 'admin/my_schemes.php' );
        include_once( plugin_dir_path( acs::$_PATH ) . 'admin/settings.php' );

        // Load language controller
        $this->controller[ 'language' ] = new acs_language();

        // Load admin controller
        $this->controller[ 'admin' ] = new acs_admin();

        // Load css controller
        $this->controller[ 'css' ] = new acs_css();

    }

    /**
     * onActivate
     */
    static function onActivate() {

    }


    /**
     * onDeactivate
     */
    static function onDeactivate() {

    }


    /**
     * onUninstall
     */
    static function onUninstall() {

    }


    /**
     * debug
     *
     * @param $message
     */
    static function debug( $message ) {
        echo '[ ACS ] :: ' . $message . '<br />';
    }

}


/**
 * Register required hooks
 */
register_activation_hook( __FILE__, array( acs::$_SLUG, 'onActivate' ) );
register_deactivation_hook( __FILE__, array( acs::$_SLUG, 'onDeactivate' ) );
register_uninstall_hook( __FILE__, array( acs::$_SLUG, 'onUninstall' ) );


/**
 * Init acs
 */
if( !isset( $GLOBALS[ acs::$_SLUG ] ) || ( isset( $GLOBALS[ acs::$_SLUG ] ) && !( $GLOBALS[ acs::$_SLUG ] instanceof acs ) ) ) {
    $GLOBALS[ acs::$_SLUG ] = new acs();
}
<?php

/**
 * Class language
 *  Handles all language operations
 */
class acs_language {

    /**
     * Variables & constants
     */
    public static $_LANG = 'acs';


    /**
     * constructor
     */
    public function __construct() {

        // Language initialize after plugins loaded
        add_action( 'plugins_loaded', array( $this, 'setup_localization' ) );

    }


    /**
     * setup_localization
     */
    public function setup_localization() {

        // Get language directory
        $language_directory = plugin_dir_path( acs::$_PATH ) . 'language/';

        // Check directory exists
        if( !is_dir( $language_directory ) ) {
            if( acs::$_DEBUG )
                acs::debug( 'Language directory does not exists.' );
            return false;
        }

        // Load text domain
        load_plugin_textdomain( acs_language::$_LANG, false, $language_directory );

    }

}
<?php

/**
 * Class admin_template
 *  Parent class of any admin template
 */
abstract class admin_template {

    /**
     * html
     *  Generate the html content of the template
     */
    abstract function html();

}
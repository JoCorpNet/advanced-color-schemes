<?php

/**
 * Class css
 *  This class creates the css file dynamically from data
 */
class acs_css {

    /**
     * Variables
     */
    private $PrimaryColors = array(
        'colorPrimary' => null,
        'colorSecondary' => null,
        'colorTertiary' => null,
        'colorQuaternary' => null
    );
    private $AdvancedColors = array(
        'colorBackground' => null,
        'colorLink' => null,
        'colorLinkActive' => null,
        'colorFormInput' => null,
        'colorButtonPrimaryBackground' => null,
        'colorButtonPrimaryBorder' => null,
        'colorButtonPrimaryText' => null,
        'colorButtonPrimaryShadowBegin' => null,
        'colorButtonPrimaryShadowEnd' => null,
        'colorButtonPrimaryHoverBackground' => null,
        'colorButtonPrimaryHoverBorder' => null,
        'colorButtonPrimaryHoverText' => null,
        'colorButtonPrimaryHoverShadow' => null,
        'colorButtonPrimaryFocusShadowBegin' => null,
        'colorButtonPrimaryFocusShadowMiddle' => null,
        'colorButtonPrimaryFocusShadowEnd' => null,
        'colorButtonPrimaryActiveBackground' => null,
        'colorButtonPrimaryActiveBorder' => null,
        'colorButtonPrimaryActiveText' => null,
        'colorButtonPrimaryActiveShadowBegin' => null,
        'colorButtonPrimaryActiveShadowMiddle' => null,
        'colorButtonPrimaryActiveShadowEnd' => null,
        'colorButtonPrimaryDisabledBackground' => null,
        'colorButtonPrimaryDisabledText' => null,
        'colorButtonPrimaryDisabledBorder' => null,
        'colorPrimaryText' => null,
        'colorPrimaryBackground' => null,
        'colorHighlightText' => null,
        'colorHighlightBackground' => null,
        'colorNotificationText' => null,
        'colorNotificationBackground' => null,
        'colorIcon' => null,
        'colorTableButtonText' => null,
        'colorTableButtonBackground' => null,
        'colorTableSwitchText' => null,
        'colorTableSwitchHoverText' => null,
        'colorTableCommentHoverBorder' => null,
        'colorTableCommentHoverText' => null,
        'colorTableCommentHoverBackground' => null,
        'colorMenuBackground' => null,
        'colorMenuIconText' => null,
        'colorMenuHoverText' => null,
        'colorMenuHoverBackground' => null,
        'colorMenuText' => null,
        'colorMenuTabActiveBackground' => null,
        'colorMenuTabActiveBorder' => null,
        'colorMenuSubBackground' => null,
        'colorMenuSubBorder' => null,
        'colorMenuSubText' => null,
        'colorMenuSubHoverText' => null,
        'colorMenuCurrentText' => null,
        'colorMenuCurrentBorder' => null,
        'colorMenuCurrentSubText' => null,
        'colorMenuCurrentSubBackground' => null,
        'colorMenuBubbleText' => null,
        'colorMenuBubbleBackground' => null,
        'colorMenuBubbleSubText' => null,
        'colorMenuBubbleSubBackground' => null,
        'colorMenuCollapseText' => null,
        'colorMenuCollapseHoverText' => null,
        'colorBarText' => null,
        'colorBarBackground' => null,
        'colorBarIcon' => null,
        'colorBarHoverText' => null,
        'colorBarHoverBackground' => null,
        'colorBarSubBackground' => null,
        'colorBarSubItemBackground' => null,
        'colorBarSubItemText' => null,
        'colorBarSearchText' => null,
        'colorBarSearchFocusText' => null,
        'colorBarSearchFocusBackground' => null,
        'colorBarSearchPlaceholderText' => null,
        'colorBarAccountBorder' => null,
        'colorBarAccountBackground' => null,
        'colorBarAccountText' => null,
        'colorBarAccountHighlightText' => null,
        'colorPointersBackground' => null,
        'colorPointersBorder' => null,
        'colorPointersText' => null,
        'colorPointersIcon' => null,
        'colorMediaProgressBackground' => null,
        'colorMediaShadowBegin' => null,
        'colorMediaShadowEnd' => null,
        'colorThemesBrowserBackground' => null,
        'colorThemesBrowserText' => null,
        'colorThemesFiltersBorder' => null,
        'colorThemesFiltersText' => null,
        'colorThemesFiltersBackground' => null,
        'colorThemesFiltersHoverBackground' => null,
        'colorThemesFiltersHoverText' => null,
        'colorWidgetsBackground' => null,
        'colorWidgetsText' => null,
        'colorCustomizeBackground' => null,
        'colorCustomizeText' => null,
        'colorSliderBackground' => null,
        'colorSliderBorder' => null,
        'colorSliderShadowBegin' => null,
        'colorSliderShadowEnd' => null,
        'colorPopupBackground' => null,
        'colorPopupBorder' => null,
        'colorPopupButtonBackground' => null,
        'colorResponsiveText' => null,
        'colorResponsiveBorder' => null,
        'colorResponsiveBackground' => null,
        'colorResponsiveStar' => null,
        'colorResponsiveOpenBackground' => null
    );


    /**
     * hex_to_rgb
     *
     * @param $hex
     * @return list($r, $g, $b)
     */
    function hex_to_rgb( $hex ) {
        return sscanf( $hex, "#%02x%02x%02x" );
    }


    /**
     * rgb_to_hex
     *
     * @param $r
     * @param $g
     * @param $b
     * @return string
     */
    function rgb_to_hex( $r, $g, $b ) {
        $hex = str_pad(dechex($r), 2, "0", STR_PAD_LEFT);
        $hex .= str_pad(dechex($g), 2, "0", STR_PAD_LEFT);
        $hex .= str_pad(dechex($b), 2, "0", STR_PAD_LEFT);
        return '#' . $hex;
    }


    /**
     * load_primary_colors
     *
     * @param $primary
     * @param $secondary
     * @param $tertiary
     * @param $quaternary
     * @param bool $isRGB
     */
    public function load_primary_colors( $primary, $secondary, $tertiary, $quaternary, $isRGB = false ) {
        $this->PrimaryColors[ 'colorPrimary' ] = ( $isRGB ) ? $this->rgb_to_hex( $primary[ 'r' ], $primary[ 'g' ], $primary[ 'b' ] ) : $primary;
        $this->PrimaryColors[ 'colorSecondary' ] = ( $isRGB ) ? $this->rgb_to_hex( $secondary[ 'r' ], $secondary[ 'g' ], $secondary[ 'b' ] ) : $secondary;
        $this->PrimaryColors[ 'colorTertiary' ] = ( $isRGB ) ? $this->rgb_to_hex( $tertiary[ 'r' ], $tertiary[ 'g' ], $tertiary[ 'b' ] ) : $tertiary;
        $this->PrimaryColors[ 'colorQuaternary' ] = ( $isRGB ) ? $this->rgb_to_hex( $quaternary[ 'r' ], $quaternary[ 'g' ], $quaternary[ 'b' ] ) : $quaternary;
    }


    /**
     * set_advanced_color
     *
     * @param $colorKey
     * @param $colorValue
     * @param bool $isRGB
     */
    public function set_advanced_color( $colorKey, $colorValue, $isRGB = false ) {
        $this->AdvancedColors[ $colorKey ] = ( $isRGB ) ? $this->rgb_to_hex( $colorValue[ 'r' ], $colorValue[ 'g' ], $colorValue[ 'b' ] ) : $colorValue;
    }


    /**
     * toString
     */
    public function __toString() {

        // Get base file
        $css_file = plugin_dir_path( acs::$_PATH ) . 'css/color.css';

        // Check base file exists
        if( !file_exists( $css_file ) ) {
            if( acs::$_DEBUG )
                acs::debug( 'Base file does not exists.' );
            return false;
        }

        // Get file content
        $css_file = file_get_contents( $css_file );
        var_dump( $css_file );


        return "dwa";
    }


}
<?php

/**
 * Class admin
 *  Class initialize the admin adrea
 */
class acs_admin {

    /**
     * variables & constants
     */
    private $Menu = array();


    /**
     * constructor
     */
    public function __construct() {

        // Add admin menus
        add_action( 'init', array( $this, 'create_post_type' ), 1 );

    }


    /**
     * create_post_type
     *  Create the acs post type for managing
     */
    public function create_post_type() {

        // Register acs post type
        register_post_type( acs::$_SLUG, array(
            'labels' => array(
                'name' => __( 'Color Groups', acs_language::$_LANG ),
                'singular_name' => __( 'Advanced Color Schemes', acs_language::$_LANG ),
                'add_new' => __( 'Add New', acs_language::$_LANG),
                'add_new_item' => __( 'Add New Color Group', acs_language::$_LANG ),
                'new_item' => __( 'New Color Group', acs_language::$_LANG ),
                'edit_item' => __( 'Edit Color Group', acs_language::$_LANG ),
                'search_items' => __( 'Search Color Groups', acs_language::$_LANG ),
                'not_found' => __( 'No Color Groups found.', acs_language::$_LANG ),
                'not_found_in_trash' => __( 'No Color Groups found in Trash.', acs_language::$_LANG )
            ),
            'public' => false,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => acs::$_SLUG,
            'rewrite' => false,
            'capability_type' => 'page',
            'hierarchical' => true,
            'menu_position' => 98,
            'menu_icon' => 'dashicons-art',
            'supports' => array( 'title', 'author', 'revisions' ),
            '_builtin' => false
        ) );


        // Register stylesheets
        add_action( 'admin_enqueue_scripts', function($hook) {
            if ( 'edit.php' != $hook ) return;

            wp_register_style( 'acs', plugins_url( 'css/acs.css', acs::$_PATH ) );
            wp_enqueue_style( 'acs' );
        } );


        // For admin only
        if( is_admin() ) {
            add_filter( 'post_updated_messages', array( $this, 'notification_messages' ) );
            add_action( 'admin_menu', array( $this, 'create_admin_menu' ) );
            add_action( 'admin_footer', array( my_schemes::$_CLASS, 'html' ) );
        }

    }


    /**
     * notification_messages
     *  Define all admin notification messages
     *
     * @param $messages
     * @return
     */
    public function notification_messages( $messages ) {

        // Define acs notification messages
        $messages['book'] = array(
            2 => __( 'Color Scheme updated.', acs_language::$_LANG ),
            3 => __( 'Color Scheme deleted.', acs_language::$_LANG ),
            4 => isset( $_GET['revision'] ) ? sprintf( __( 'Color Scheme restored to revision from %s', acs_language::$_LANG ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
            5 => __( 'Color Scheme saved.', acs_language::$_LANG ),
            6 => __( 'Color Scheme draft updated.', acs_language::$_LANG )
        );

        // Return messages
        return $messages;

    }


    /**
     * create_admin_menu
     *  Create administration menus
     */
    public function create_admin_menu() {

        // Register 'Settings' sub menu
        $Menu[ settings::$_SLUG ] = add_submenu_page(
            'edit.php?post_type=' . acs::$_SLUG,        // Parent: acs-my_schemes (My schemes)
            __( 'Settings', acs_language::$_LANG ),     // Page title: Settings
            __( 'Settings', acs_language::$_LANG ),     // Menu title: Settings
            'manage_options',                           // Capabilities: manage_options
            settings::$_SLUG,                           // Slug: acs-settings
            array( settings::$_CLASS, 'html' )          // Content callback: html from settings class
        );

    }


    /**
     * create_screen_menu
     *  Create a help or options menu to this screen
     *
     * @param $slug
     * @param $callback
     */
    private function create_screen_menu( $slug, $callback ) {
        add_action("load-$slug", $callback );
    }

}